//
//  HikVisionExt.h
//  HikVisionExt
//
//  Created by Dan Kalinin on 8/23/20.
//

#import <HikVisionExt/HveMain.h>
#import <HikVisionExt/HveSchema.h>
#import <HikVisionExt/HveClient.h>
#import <HikVisionExt/HveBrowser.h>
#import <HikVisionExt/HveInit.h>

FOUNDATION_EXPORT double HikVisionExtVersionNumber;
FOUNDATION_EXPORT const unsigned char HikVisionExtVersionString[];
