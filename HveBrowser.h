//
//  HveBrowser.h
//  HikVisionExt
//
//  Created by Dan on 18.09.2021.
//

#import "HveMain.h"
#import "HveSchema.h"

@class HveBrowser;

@protocol HveBrowserDelegate <NSObject>

@optional
- (void)hveBrowser:(HveBrowser *)sender endpointFound:(HveEndpoint *)endpoint;
- (void)hveBrowser:(HveBrowser *)sender endpointRemoved:(NSString *)name;

@end

@interface HveBrowserDelegates : NseArray <HveBrowserDelegate>

@end

@interface HveBrowser : NseNetServiceBrowser <NSNetServiceBrowserDelegate, NSNetServiceDelegate>

@property NSString *type;
@property HveBrowserDelegates *delegates;
@property NSMutableSet<NSNetService *> *services;

+ (instancetype)browserWithType:(NSString *)type;

- (void)start;

@end
